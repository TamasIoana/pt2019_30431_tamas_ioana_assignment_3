package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import com.mysql.jdbc.Statement;

import model.Client;
import model.Reflection;
import presentation.TableRows;
import start.ConnectionFactory;

/*
 * 
The Data Access Object manages the connection with the data source to obtain and store data.
It abstracts the underlying data access implementation for the Business Object to enable transparent access to the data source.
Therefore,this class contains the following methods for the Client table: insert,delete,update and retrieve data from database
 */
public class ClientDAO {
	private static Connection con;
	
	
	public ClientDAO() {
		try {
		 con =DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/shop?useSSL=true","root","rihanna");
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}	
	}
	//display data in jtable
	//fill array list with the data 
	public static ArrayList<Client> getClient(){
		ArrayList<Client>toReturn=new ArrayList<Client>();
		String stmtString="SELECT * FROM clients WHERE ClientID=?";
		 // PreparedStatements can use variables and are more efficient
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int ClientID=rs.getInt("ClientID");
				String name=rs.getString("name");
				
				Client newClient=new Client(ClientID, name);
				
				newClient.setClientID(ClientID);
				newClient.setClientName(name);
				toReturn.add(newClient);
				
			}
			
			
		}catch(SQLException e) {
			System.out.println("SQLException "+e.getMessage());
			
		}
		return toReturn;
		
	}
	
	
	//inserting a new client
	public void insertClient(int ClientID,String name) {
		String stmtString="INSERT INTO clients  (ClientID,name) VALUES(?,?) ";
		PreparedStatement stmt=null;
		try {
			stmt=con.prepareStatement(stmtString);
			 // Parameters start with 1
			stmt.setInt(1,ClientID);
			stmt.setString(2,name);
			stmt.executeUpdate();
		
		}catch(SQLException e) {
			System.out.println("SQLExceptionINSERT"+e.getMessage());
			
		}
		
	}
	
	
	//update client table
	public void updateClient(int ClientID,String name) {
		String stmtString="UPDATE clients SET name=? WHERE ClientID=?";
		PreparedStatement stmt=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setString(1, name);
			stmt.setInt(2,ClientID);
			stmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLExceptionUPDATE"+e.getMessage());
			
		}
		
	}
	
	
	//delete cloient from table
	public void deleteClient(int ClientID) {
		String deleteStatement="DELETE FROM clients WHERE ClientID=?";
		PreparedStatement deletestmt=null;
		
		try {
			deletestmt=con.prepareStatement(deleteStatement);
			deletestmt.setInt(1, ClientID);
			deletestmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	
	public static ArrayList<Client> findAllClients(){

		ArrayList<Client> allClients = new ArrayList<Client>();
		 String statement = "SELECT * FROM clients";
		PreparedStatement findAll = null;
		Connection conn = ConnectionFactory.getConnection();
		ResultSet rs = null;

		try {
			findAll = conn.prepareStatement(statement);
			rs = findAll.executeQuery();

			while(rs.next()) {
				int ClientID = rs.getInt(1);
				String name = rs.getString(2);
			
				
				Client newClient = new Client(ClientID, name);
				
				allClients.add(newClient);			
			}
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return allClients;
	}
	
	
public static JTable buildTable(ArrayList<Client> clientList){
		
		String columns[] = new String[2];//the headers
		String values[] = new String[5];//the data
	
		Reflection.retrieveFieldNames(clientList.get(0), columns);
		
		int nrRows = TableRows.countRowsClient(clientList.get(0));
		
		String data[][] = new String[nrRows][2];
		
		for(int j = 0; j < clientList.size(); j++) {
			Reflection.retrieveProperties(clientList.get(j), values);
			data[j][0] = values[0];
			data[j][1] = values[1];	
		
		}
		
		JTable jt = new JTable(data, columns);
		return jt;
     }

	
	
}
