package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import model.Product;

public class ProductDAO {

	private Connection con;
	//private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	
	public ProductDAO() {
		try {
			 con =DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/shop?useSSL=true","root","rihanna");
				
			}catch(SQLException e) {
				System.out.println("SQLException"+e.getMessage());
			}	
	}
	
	public ArrayList<Product> getProduct(){
		ArrayList<Product>toReturn=new ArrayList<Product>();
		String stmtString="SELECT * FROM product ";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			rs=stmt.executeQuery();
			
			while(rs.next()) {
				int productID=rs.getInt("productID");
				String pname=rs.getString("pname");
				Float pprice=rs.getFloat("pprice");
				
				
				Product newProduct=new Product();
				newProduct.setProductID(productID);
				newProduct.setProductName(pname);
				newProduct.setProductPrice(pprice);
				toReturn.add(newProduct);
				
			}
			
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		return toReturn;
		
	}
	
	public void createProduct(int productID,String pname,Float pprice) {
		String stmtString="INSERT INTO product (productID,pname,pprice) VALUES (?,?,?) ";
		PreparedStatement stmt=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1, productID);
			stmt.setString(2,pname);
			stmt.setFloat(3,pprice);
			stmt.executeUpdate();
		
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		
	}
	
	public void updateProduct(int productID,String pname,Float pprice) {
		String stmtString="UPDATE product SET pname=?,pprice=? WHERE productID=?";
		PreparedStatement stmt=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setString(1, pname);
			stmt.setFloat(2,pprice);
			stmt.setInt(3,productID);
			stmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		
	}
	
	public void deleteProduct(int productID) {
		String deleteStatement="DELETE FROM product WHERE productID=?";
		PreparedStatement deletestmt=null;
		
		try {
			deletestmt=con.prepareStatement(deleteStatement);
			deletestmt.setInt(1, productID);
			deletestmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	
	
	
	
	
}

