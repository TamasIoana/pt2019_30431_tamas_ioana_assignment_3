package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import model.Warehouse;

public class WarehouseDAO {
	
	private Connection con;
	
	public WarehouseDAO() {
		try {
			 con =DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/shop?useSSL=true","root","rihanna");
				
			}catch(SQLException e) {
				System.out.println("SQLException"+e.getMessage());
			}	
	}
	
	public void insertProduct(int productID) {
		String stmtString="INSERT INTO warehouse ";
		PreparedStatement stmt=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,productID);
			stmt.executeUpdate();
			

		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		
	}
	
	public ArrayList<Warehouse> findStock(){
		ArrayList<Warehouse>toReturn=new ArrayList<Warehouse>();
		String stmtString="SELECT * FROM warehouse WHERE warehouseID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int warehouseID=rs.getInt("warehouseID");
				int productID=rs.getInt("productID");
				int stock=rs.getInt("stock");
				
				Warehouse newWarehouse=new Warehouse();
				 newWarehouse.setWarehouseID(warehouseID);
				 newWarehouse.setProductID(productID);
				 newWarehouse.setStock(stock);
				 
				 toReturn.add( newWarehouse);
			}
			
		}catch(SQLException e){
			System.out.println("SQLException"+e.getMessage());
			
		}
		return toReturn;
	}
	
	
	public void updateStock(int productID,int stock) {
		String stmtString="UPDATE warehouse SET stock=? WHERE productID=?";
		PreparedStatement stmt=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1, stock);
			stmt.setInt(2,productID);
			int x=stmt.executeUpdate();
			if(x>0) 
				System.out.println("Updated Succesfully");
			else 
				System.out.println("Error Occured");
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	public void deleteProductFromStock(int productID) {
		String deleteStatement="DELETE FROM warehouse WHERE productID=?";
		PreparedStatement deletestmt=null;
		
		try {
			deletestmt=con.prepareStatement(deleteStatement);
			deletestmt.setInt(1, productID);
			deletestmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	public void decreaseStock(int productID,int quantity) {
		int stock=0;
		String stmtString="SELECT * FROM warehouse WHERE productID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,productID);
			rs=stmt.executeQuery();
			while(rs.next()) {
				stock=rs.getInt("stock");
			}
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());	
		}
		
		int newStock=stock-quantity;
		
		String stmtStringNew="UPDATE warehouse SET stock=? WHERE productID=?";
		PreparedStatement stmtNew=null;
		//ResultSet rsNew=null;
		try {
			stmtNew=con.prepareStatement(stmtStringNew);
			stmtNew.setInt(1, newStock);
			stmtNew.setInt(2, productID);
			stmtNew.executeUpdate();
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());	
		}
	}
	
	public void increaseStock(int productID,int quantity) {
		int stock=0;
		String stmtString="SELECT * FROM warehouse WHERE productID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,productID);
			rs=stmt.executeQuery();
			while(rs.next()) {
				stock=rs.getInt("stock");
			}
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());	
		}
		
		int newStock=stock+quantity;
		
		String stmtStringNew="UPDATE warehouse SET stock=? WHERE productID=?";
		PreparedStatement stmtNew=null;
		//ResultSet rsNew=null;
		try {
			stmtNew=con.prepareStatement(stmtStringNew);
			stmtNew.setInt(1, newStock);
			stmtNew.setInt(2, productID);
			stmtNew.executeUpdate();
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());	
		}
	}
	
	public Boolean isEnough(int productID,int quantity) {
		boolean ok=false;
		String stmtString="SELECT * FROM warehouse WHERE productID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,productID);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int stock=rs.getInt("stock");
				if(stock>=quantity) {
					ok=true;
				}
			}
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}
		return ok;
				
				
	}

}
