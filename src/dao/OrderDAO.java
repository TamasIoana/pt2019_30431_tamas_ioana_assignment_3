package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import model.Order;

public class OrderDAO {
	private Connection con;
	
	public OrderDAO() {
		try {
		 con =DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/shop?useSSL=true","root","rihanna");
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}	
	}
	
	public ArrayList<Order> getOrder(){
		ArrayList<Order>toReturn=new ArrayList<Order>();
		String stmtString="SELECT * FROM order WHERE id=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			rs=stmt.executeQuery();
			while(rs.next()) {
				int orderID=rs.getInt("orderID");
				int orderProductID=rs.getInt("orderProductID");
				int orderClientID=rs.getInt("orderClientID");
				int quantity=rs.getInt("quantity");
				
				Order newOrder=new Order();
				newOrder.setOrderClientID(orderClientID);
				newOrder.setOrderID(orderID);
				newOrder.setOrderProductID(orderProductID);
				newOrder.setQuantity(quantity);
				
				toReturn.add(newOrder);
			}
			
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		return toReturn;
		
	}
	
	public void newOrder(int orderID,int orderClientID,int orderProductID,int quantity) {
		String stmtString="INSERT INTO order (orderID,orderProductID,orderClientID,quantity) VALUES(?,?,?,?) ";
		PreparedStatement stmt=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,orderID );
			stmt.setInt(2,orderProductID);
			stmt.setInt(3,orderClientID);
			stmt.setInt(4,quantity);
			stmt.executeUpdate();
			

		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	public void updateOrder(int orderID,int orderClientID,int orderProductID,int quantity) {
		String stmtString="UPDATE order SET orderProductID=?,orderClientID=?,quantity=? WHERE orderID=?";
		PreparedStatement stmt=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,orderID);
			stmt.setInt(2,orderProductID);
			stmt.setInt(3,orderClientID);
			stmt.setInt(4,quantity);
			stmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
		
	}
	
	
	public void deleteOrder(int orderID ){
		String deleteStatement="DELETE FROM order WHERE orderID=?";
		PreparedStatement deletestmt=null;
		
		try {
			deletestmt=con.prepareStatement(deleteStatement);
			deletestmt.setInt(1, orderID);
			deletestmt.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
			
		}
	}
	
	public int getQuantity(int orderID) {
		int q=0;//quantity
		
		String stmtString="SELECT * FROM order WHERE orderID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,orderID);
			rs=stmt.executeQuery();
			while(rs.next()) {
				q=rs.getInt("quantity");
				
			}
			
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}
		
		System.out.println("hello here is something "+q+"id"+orderID);
		return q;
	}
	
	public void updateQuantity(int warehouseID,int stock) {
		
		String stmtString="UPDATE warehouse SET stock WHERE warehouseID=?";
		PreparedStatement stmt=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,stock);
			stmt.setInt(2,warehouseID);
			stmt.executeQuery();
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}
	}
	
	public int getProdID(int orderID) {
		int product=0;
		String stmtString="SELECT * FROM order WHERE orderID=?";
		PreparedStatement stmt=null;
		ResultSet rs=null;
		try {
			stmt=con.prepareStatement(stmtString);
			stmt.setInt(1,orderID);
			rs=stmt.executeQuery();
			while(rs.next()) {
				product=rs.getInt("orderProductID");
				
			}
			
		}catch(SQLException e) {
			System.out.println("SQLException"+e.getMessage());
		}
		return product;
	}

}
