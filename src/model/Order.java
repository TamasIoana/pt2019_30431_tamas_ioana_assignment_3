package model;
/*
 * This class containt attributes for the order of the client 
 * @orderID is the identification number for each order
 * @orderProductID is the identification number for the ordered product
 * @orderClinetID is the identification number for the client that made the order 
 * @quntity is the quantity of ordered products
 * 
 */

public class Order {
	private int orderID;
	private int orderProductID;
	private int orderClientID;
	private int quantity;
	
	public int getOrderID() {
		return orderID;
	}
	
	public int getOrderProductID() {
		return orderProductID;
	}
	
	public int getOrderClientID() {
		return orderClientID;
	}
	
	public int getOrderQuantity() {
		return quantity;
	}
	
	public void setOrderID(int orderID) {
		this.orderID=orderID;
	}
	
	public void setOrderProductID(int orderProductID) {
		this.orderProductID=orderProductID;
	}
	
	public void setOrderClientID(int orderClientID) {
		this.orderClientID=orderClientID;
	}
	
	public void setQuantity(int quantity) {
		this.quantity=quantity;
	}
	

}
