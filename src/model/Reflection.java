package model;

import java.lang.reflect.Field;

public class Reflection {
	
	//retrieving fields' values
	public static void retrieveProperties(Object object, String[] values) {
		int i = 0;
		for(Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(object);
				values[i] = value.toString();
				i++;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	//retrieving the fields' names
	public static void retrieveFieldNames(Object object, String[] headers) {
		int i = 0;
		for(Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			
				headers[i] = field.getName();
				i++;
		}
	}
}
