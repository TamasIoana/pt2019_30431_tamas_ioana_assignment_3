package model;

import java.util.List;

public class Warehouse {
	
	private int warehouseID;
	private int productID;
	private int stock;
	private List<Product>products;
	
	//get methods
	public int getWarehouseID() {
		return warehouseID;
	}
	
	public List<Product> getWProducts(){
		return products;
	}
	
	public int getProductID() {
		return productID;
	}
	public int getStock() {
		return stock;
	}
	
	//set methods 
	public void setWarehouseID(int warehouseID) {
		this.warehouseID=warehouseID;
	}
	public void setWProducts(List<Product>products) {
		this.products=products;
	}
	public void setProductID(int productID) {
		this.productID=productID;
	}
	public void setStock(int stock) {
		this.stock=stock;
	}
	
	
}