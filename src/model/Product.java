package model;

/*
 * This class contains getters and setters for the attributes of the products 
 * @productID-integer identifier for the product
 * @pname-name of the product
 * @pprice-the price of the product
 */
public class Product {
	
	private int productID;
	private String pname;
	private float pprice;
	
	public int getProductID() {
		return productID;
	}
	
	public String getProductName() {
		return pname;
	}
	
	public Float getProductPrice() {
		return pprice;
	}
	public void setProductID(int productID) {
		this.productID=productID;
	}
	public void setProductName(String pname) {
		this.pname=pname;
	}
	public void setProductPrice(Float pprice) {
		this.pprice=pprice;
	}

}
