package model;


/*
 * This class containt attributes for the client
 * @clientID- an integer as a identifier for the client 
 * @name - string that will be the name of the client 
 */
public class Client {
	private int clientID;
	private String name;
	
	
	public Client(int clientID,String name) {
		this.clientID=clientID;
		this.name=name;
	}
	
	public int getClientID() {
		return clientID;
	}
	
	public String getClientName() {
		return name;
	}
	
	public void setClientID(int clientID) {
		this.clientID=clientID;
	}
	
	public void setClientName(String name) {
		this.name=name;
	}
	
	@Override 
	public String toString() {
		return "Client :"+name+" with the ID:"+clientID;
	}

}
