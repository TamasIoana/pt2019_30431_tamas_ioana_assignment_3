package bll;

import java.util.ArrayList;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import dao.WarehouseDAO;
import model.Client;
import model.Order;
import model.Product;

/*
 * 
 * This class contains operations that will be transmited to the database 
 *
 */
public class shopManagement  {
	
	//PRODUCT OPERATIONS CREATE DELETE UPDATE
	
	//add product to the stock in the warehouse 
	public void insertProduct(Product prod) {
	
		String pname=prod.getProductName();
		Float pprice=prod.getProductPrice();
		int pid=prod.getProductID();
		
		//create new product of type ProductDAO
		ProductDAO newProductDAO=new ProductDAO();
		newProductDAO.createProduct(pid,pname,pprice);
		
		//new warehouse object where the product will be inserted
		WarehouseDAO newWarehouseDAO=new WarehouseDAO();
		newWarehouseDAO.insertProduct(pid);
		
	}
	
	//delete product using product id
	public void deleteProduct(int pid) {
		ProductDAO newProductDAO=new ProductDAO();
		newProductDAO.deleteProduct(pid);
		
		//new warehouse object from where the product will be deleted
		WarehouseDAO newWarehouseDAO=new WarehouseDAO();
		newWarehouseDAO.deleteProductFromStock(pid);
		
	
	}
	
	//update product 
	public void updateProduct(Product prod) {
		int pid=prod.getProductID();
		String pname=prod.getProductName();
		Float pprice=prod.getProductPrice();
		
		//create new product of type ProductDAO
		ProductDAO newProductDAO=new ProductDAO();
		newProductDAO.updateProduct(pid,pname,pprice);
		}
	
	public ArrayList<Product> getAllProducts(){
		ArrayList<Product> returnStatement = new ArrayList<Product>();
		ProductDAO productDAO = new ProductDAO();
		returnStatement = productDAO.getProduct();
		return returnStatement;
	}
	
	
	//CLIENT OPERATIONS CREATE DELETE UPDATE
	public void insertClient(Client client) {
		String cname=client.getClientName();
		int cid=client.getClientID();
		ClientDAO newClientDAO=new ClientDAO();
		newClientDAO.insertClient(cid,cname);
	}
	
	public void deleteClient(int cid) {
		ClientDAO newClientDAO=new ClientDAO();
		newClientDAO.deleteClient(cid);
		
	}
	
	public void updateClient(Client client) {
		int cid=client.getClientID();
		String cname=client.getClientName();
		ClientDAO newClientDAO=new ClientDAO();
		newClientDAO.updateClient(cid, cname);
	}
	
	/*
	public ArrayList<Client> getAllClients(){
		ArrayList<Client> returnStatement = new ArrayList<Client>();
		//ClientDAO clientDAO = new ClientDAO();
		returnStatement = ClientDAO.getClient();
		return returnStatement;
		
	}
	*/
	public static ArrayList<Client> getAllClients(){
		ArrayList<Client> findAll = new ArrayList<Client>();
		findAll = ClientDAO.findAllClients();
		return findAll;
    }
	
	
	
	//ORDER OPERATIONS
	
	public void insertOrder(Order order) {
		int orderID=order.getOrderID();
		int orderClientID=order.getOrderClientID();
		int orderProductID=order.getOrderProductID();
		int orderQuant=order.getOrderQuantity();
		boolean ok=false;
		OrderDAO newOrderDAO=new OrderDAO();
		//newOrderDAO.newOrder(orderClientID, orderProductID,orderQuant);
		
		WarehouseDAO newWarehouseDAO=new WarehouseDAO();
		ok=newWarehouseDAO.isEnough(orderProductID, orderQuant);
		if(ok) {
			newOrderDAO.newOrder(orderID,orderClientID, orderProductID,orderQuant);
			newWarehouseDAO.decreaseStock(orderProductID, orderQuant);
		}
		
	}
	
	public void updateOrder(Order order ) {
		int orderID=order.getOrderID();
		int orderProductID=order.getOrderProductID();
		int orderQuant=order.getOrderQuantity(); 
		int orderClientID=order.getOrderClientID();
		boolean ok=false;
		OrderDAO newOrderDAO=new OrderDAO();
		//newOrderDAO.newOrder(orderClientID, orderProductID,orderQuant);
		WarehouseDAO newWarehouseDAO=new WarehouseDAO();
		ok=newWarehouseDAO.isEnough(orderProductID, orderQuant);
		if(ok) {
			newOrderDAO.updateOrder(orderID,orderClientID, orderProductID,orderQuant);
			newWarehouseDAO.decreaseStock(orderProductID, orderQuant);
	
		}
	}
	
	public void removeOrder(int orderID) {
		OrderDAO newOrderDAO=new OrderDAO();
		int quantity=newOrderDAO.getQuantity(orderID);
		int orderProductID=newOrderDAO.getProdID(orderID);
		newOrderDAO.deleteOrder(orderID);
		
		WarehouseDAO newWarehouseDAO=new WarehouseDAO();
		newWarehouseDAO.increaseStock(orderProductID, quantity);	
	}
	
	public ArrayList<Order> getAllOrders(){
		ArrayList<Order> returnStatement = new ArrayList<Order>();
		OrderDAO orderDAO =  new OrderDAO();
		returnStatement = orderDAO.getOrder();
		return returnStatement;
	}
	
	
	



}
