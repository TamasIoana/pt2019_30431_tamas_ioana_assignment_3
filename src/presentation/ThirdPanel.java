package presentation;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class ThirdPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Box mainBox, hBox1, hBox2, hBox3,hBox4,hBox6, hBox7,hBox8;
			

	private JLabel jlOrderID, jlProdID,jlClientID,jlQuantity;
	private JTextField jtOrderID, jtProdID,jtClientID,jtQuantity;
	private JButton bAdd, bDelete, bUpdate, bView;

	String[] orderColumns= {"ID","ClientID","ProductID","Quantity"};
	String[][] orderData= {{" "," "," "," "," "}};
	

	public ThirdPanel() {
		super(new FlowLayout());
		initWidgets();
		addWidgets();
		setBackground(new Color(194, 230, 248));
	}

	private void initWidgets() {
		mainBox = Box.createVerticalBox();

		hBox1 = Box.createHorizontalBox();
		hBox2 = Box.createHorizontalBox();
		hBox3 = Box.createHorizontalBox();
		hBox4 = Box.createHorizontalBox();
		hBox6 = Box.createHorizontalBox();
		hBox7 = Box.createHorizontalBox();
		hBox8 = Box.createHorizontalBox();
	

		jlOrderID = new JLabel("Enter ID:        ");
		jlProdID = new JLabel("Enter ProductID: ");
		jlClientID = new JLabel("Enter ClientID:  ");
		jlQuantity = new JLabel("Enter Quantity:  ");
	

		jtOrderID = new JTextField(19);
		jtProdID = new JTextField(19);
		jtClientID = new JTextField(19);
		jtQuantity = new JTextField(19);
		
		
	
		jtOrderID.setHorizontalAlignment(JTextField.RIGHT);
		jtProdID.setHorizontalAlignment(JTextField.RIGHT);
		jtClientID.setHorizontalAlignment(JTextField.RIGHT);
		jtQuantity.setHorizontalAlignment(JTextField.RIGHT);
		

		bAdd = new JButton("ADD");
		bDelete = new JButton("DELETE");
		bUpdate= new JButton("UPDATE");
		bView = new JButton("VIEW");
		
	

	}

	private void addWidgets() {
		hBox1.add(jlOrderID);
		hBox1.add(jtOrderID);
		hBox2.add(jlProdID);
		hBox2.add(jtProdID);
		hBox3.add(jlClientID);
		hBox3.add(jtClientID);
		hBox4.add(jlQuantity);
		hBox4.add(jtQuantity);
		
		hBox6.add(Box.createHorizontalStrut(82));
		hBox6.add(bAdd);
		hBox6.add(Box.createHorizontalStrut(5));
		hBox6.add(bDelete);
		hBox7.add(Box.createHorizontalStrut(5));
		hBox7.add(bUpdate);
		hBox7.add(Box.createHorizontalStrut(5));
		hBox7.add(bView);
		hBox7.add(Box.createHorizontalStrut(20));
	

		mainBox.add(hBox1);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox2);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox3);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox4);
		mainBox.add(Box.createVerticalStrut(5));
		
		mainBox.add(hBox6);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox7);
		mainBox.add(hBox8);
	

		add(mainBox);
	}

	public void addActionListener(ActionListener a) {
		bAdd.addActionListener(a);
		bDelete.addActionListener(a);
		bUpdate.addActionListener(a);
		bView.addActionListener(a);
		
	}


	
	public JButton getButtonAdd() {
		return bAdd;
	}

	public JButton getButtonDelete() {
		return bDelete;
	}

	public JButton getButtonUpdate() {
		return bUpdate;
	}

	public JButton getButtonView() {
		return bView;
	}


	public JTextField getTextFieldOrderID() {
		return jtOrderID;
	}

	public JTextField getTextFieldProdID() {
		return jtProdID;
	}
	public JTextField getTextFieldClientID() {
		return jtClientID;
	}
	public JTextField getTextFielQuantity() {
		return jtQuantity;
	}
	


	
	

}