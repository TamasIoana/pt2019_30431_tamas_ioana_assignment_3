package presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import bll.shopManagement;
import dao.ClientDAO;
import model.Client;

import model.Order;
import model.Product;

public class Controller implements ActionListener,ChangeListener {

	

	private String[][] clientsData;
	private String[][] productsData;
	private String[][] ordersData;
	//private String[] col;
	

	
	//private List<Client> client = new ArrayList<Client>();
	private List<Product> products = new ArrayList<Product>();
	private List<Order> orders = new ArrayList<Order>();

	
	private String[] clientsNameString = {};
	shopManagement operations=new shopManagement();
	
	private FirstPanel panel1;
	private SecondPanel panel2;
	private ThirdPanel panel3;
	private ShopInterface screen;
	
	
	private JTable clientsTable;
	ArrayList<Client> clients = ClientDAO.findAllClients();
    private JScrollPane scrollPane;
    private JPanel table = new JPanel();
    
    private JFrame frame = new JFrame("Client Processing");
	
	public Controller() {
		
		
		screen=new ShopInterface("Online Shop");
		panel1=screen.getPanel1();
		panel2=screen.getPanel2();
		panel3=screen.getPanel3();
		
		screen.getTabbedPane().addChangeListener(this);
		panel1.addActionListener(this);
		panel2.addActionListener(this);
		panel3.addActionListener(this);
		screen.setVisible(true);
	
		
		
		}

	
	public void updateTableClients() { 
		shopManagement op = new shopManagement();
		this.clients = op.getAllClients();
		int size = clients.size();
		String[][] row = new String[size+2][6];
		for(int line=0;line<size;line++){
			//String c0 = clients.get(line).getClientID().toString();
			String c0= Integer.toString(clients.get(line).getClientID());
			String c1 = clients.get(line).getClientName();

			row[line][0] = c0;
			row[line][1] = c1;
			System.out.println("line"+row[line][1]+" "+line);
		}
		this.clientsData = row;
		//this.clientsTableModel.setData(row);
		//this.repaint();
		System.out.println("HERE");

	}
	
	public void updateTableProducts() { 
		shopManagement op = new shopManagement();
		this.products = op.getAllProducts();
		int size = products.size();
		String[][] row = new String[size+2][6];
		for(int line=0;line<size;line++){
			String c0 = Integer.toString(products.get(line).getProductID());
			String c1 = products.get(line).getProductName();
			String c2 = products.get(line).getProductPrice().toString();

			row[line][0] = c0;
			row[line][1] = c1;
			row[line][2] = c2;
			//System.out.println("line"+row[line][1]+" "+line);
		}
		this.productsData = row;
		//this.productsTableModel.setData(row);
		//this.repaint();

	}
	
	public void updateClientList() { 
		shopManagement op = new shopManagement();
		this.clients = op.getAllClients();
		int size = clients.size();
		String[] row = new String[size+2];
		for(int i=0;i<size;i++) {
			String c1 = clients.get(i).getClientName();
			row[i]=c1;
			

		}
		
	}
	

	public void updateTableOrders() { 
		shopManagement op = new shopManagement();
		this.orders = op.getAllOrders();
		int size = orders.size();
		String[][] row = new String[size+2][6];
		for(int line=0;line<size;line++){
			String c0 =Integer.toString(orders.get(line).getOrderID());
			String c1 = Integer.toString(orders.get(line).getOrderProductID());
			String c2 =Integer.toString(orders.get(line).getOrderClientID());
			String c3 = Integer.toString(orders.get(line).getOrderQuantity());

			row[line][0] = c0;
			row[line][1] = c1;
			row[line][2] = c2;
			row[line][3] = c3;
		
		}
	
	}

@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	
	
	//PANEL ONE BUTTONS EVENTS
	if(e.getSource()==panel1.getButtonAdd()) {
		String client_name=panel1.getTextFieldName().getText();
		int client_id;
		client_id=Integer.parseInt(panel1.getTextFieldID().getText());

		Client newClient=new Client(client_id,client_name);
		newClient.setClientName(client_name);
		newClient.setClientID(client_id);
		operations.insertClient(newClient);
		updateTableClients();
		//updateClientList();
	}
	if(e.getSource()==panel1.getButtonDelete()) {
		int client_id;
		client_id=Integer.parseInt(panel1.getTextFieldID().getText());
		operations.deleteClient(client_id);
		updateTableClients();
		//updateClientList();
	}
	
	
	if (e.getSource() == panel1.getButtonUpdate()) {
		Integer client_id=Integer.parseInt(panel1.getTextFieldID().getText());
		String client_name=panel1.getTextFieldName().getText();
		Client newClient=new Client(client_id,client_name);
		//newClient.setClientName(client_name);
		//newClient.setClientID(client_id);
		operations.updateClient(newClient);
		
		updateTableClients();
		//updateClientList(); 
		
		
	}
	else if (e.getSource() == panel1.getButtonView()) {
		
		//System.out.println("the button is clicked ");
		frame.setSize(700, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);
		

		table.setLayout(new FlowLayout());
		
		clientsTable = ClientDAO.buildTable(clients);
		
		scrollPane = new JScrollPane(clientsTable);
		
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVisible(true);
		
		table.add(scrollPane);
		frame.setVisible(true);
		frame.add(table, BorderLayout.SOUTH);
	}

	

	
	
	//PANEL TWO BUTTONS EVENTS
	if(e.getSource()==panel2.getButtonAdd()) {
		String product_name=panel2.getTextFieldNameP().getText();
		int product_id;
		product_id=Integer.parseInt(panel2.getTextFieldID().getText());
		Float product_price =  Float.parseFloat(panel2.getTextFieldPrice().getText());
		//Client newClient=new Client(client_id,client_name);
		Product newProduct=new Product();
		newProduct.setProductID(product_id);
		newProduct.setProductName(product_name);
		newProduct.setProductPrice(product_price);
		operations.insertProduct(newProduct);
		updateTableProducts();
		//updateClientList();
	}
	if(e.getSource()==panel2.getButtonDelete()) {
		int product_id;
		product_id=Integer.parseInt(panel2.getTextFieldID().getText());
		operations.deleteProduct(product_id);
		updateTableProducts();
		//updateClientList();
	}
	
	
	
	//PANEL THREE BUTTONS EVENTS 
	if(e.getSource()==panel3.getButtonAdd()) {
		Integer order_id=Integer.parseInt(panel3.getTextFieldOrderID().getText());
		Integer order_client_id =  Integer.parseInt(panel3.getTextFieldClientID().getText());
		Integer order_product_id =   Integer.parseInt(panel3.getTextFieldProdID().getText());
		Integer order_quantity = Integer.parseInt(panel3.getTextFielQuantity().getText());
		Order newOrder=new Order();
		newOrder.setOrderID(order_id);
		newOrder.setOrderProductID(order_product_id);
		newOrder.setOrderClientID(order_client_id);
		newOrder.setQuantity(order_quantity);
		operations.insertOrder(newOrder);
		updateTableOrders();
		//updateClientList();
	}
	
	
	
}

@Override
public void stateChanged(ChangeEvent arg0) {
	// TODO Auto-generated method stub
	
}

private void addClients() {
	boolean IDAlreadyExists = false;
	boolean AllFieldsAreFilled = false;
	int id = 0;
	String cname=null;
	Client c;

	if (!panel1.getTextFieldID().getText().trim().contentEquals("")
			&& !panel1.getTextFieldName().getText().trim().contentEquals("")
		) {
		AllFieldsAreFilled = true;
	}

	if (AllFieldsAreFilled) {
		try {
			id= Integer
					.parseInt(panel1.getTextFieldID().getText().trim());
			cname = panel1.getTextFieldName().getText().trim();

		
	
				c= new Client(id,panel1.getTextFieldName().getText().trim());

			

				operations.insertClient(c);
				panel1.getTextFieldID().setText("");
				panel1.getTextFieldName().setText("");
			

			
			
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(screen,
					"Isbn or Price is not a number!");
		}
	} else {
		JOptionPane.showMessageDialog(screen,
				"Please fill out all non-optional fields");
	}
}


}
