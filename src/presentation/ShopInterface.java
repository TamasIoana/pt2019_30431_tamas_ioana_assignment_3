package presentation;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class ShopInterface extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private FirstPanel p1;
	private SecondPanel p2;
	private ThirdPanel p3;
	private JTabbedPane jtb;
	private String filler;

	public ShopInterface(String title){
		super(title);
		jtb = new JTabbedPane();
		p1= new FirstPanel();
		p2 = new SecondPanel();
		p3=new ThirdPanel();
		
		filler = "      "; // 6 spaces
		jtb.addTab(filler + filler + " Client " + filler + filler, p1);
		jtb.addTab(filler + " Product" + filler, p2);
		jtb.addTab(filler + " Order" + filler, p3);
		
		
		add(jtb);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(400, 460);
		setResizable(false);
	}
	
	public FirstPanel getPanel1(){
		return p1;
	}
	
	public SecondPanel getPanel2(){
		return p2;
	}
	public ThirdPanel getPanel3(){
		return p3;
	}
	
	public JTabbedPane getTabbedPane(){
		return jtb;
	}
	
	public String getFiller(){
		return filler;
	}
	
}
