package presentation;




import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class SecondPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Box mainBox, hBox1, hBox2, hBox3, hBox6, hBox7,hBox8;
	private JLabel jlID, jlNameP,jlPrice;
	private JTextField jtID, jtNameP,jtPrice;
	private JButton bAdd, bDelete, bUpdate, bView;

	
	String[] productColumns= {"ID","Name","Price"};
	String[][] productData= {{" "," "," "}};
	
	public SecondPanel() {
		super(new FlowLayout());
		initWidgets();
		addWidgets();
		setBackground(new Color(194, 230, 248));
	}

	private void initWidgets() {
		mainBox = Box.createVerticalBox();

		hBox1 = Box.createHorizontalBox();
		hBox2 = Box.createHorizontalBox();
		hBox3 = Box.createHorizontalBox();
	
		hBox6 = Box.createHorizontalBox();
		hBox7 = Box.createHorizontalBox();
		hBox8 = Box.createHorizontalBox();
	

		jlID = new JLabel("Enter ID:       ");
		jlNameP = new JLabel("Enter Name:  ");
		jlPrice = new JLabel("Enter price:  ");
	

		jtID = new JTextField(19);
		jtNameP = new JTextField(19);
		jtPrice = new JTextField(19);
		
		
	
		jtID.setHorizontalAlignment(JTextField.RIGHT);
		jtNameP.setHorizontalAlignment(JTextField.RIGHT);
		jtPrice.setHorizontalAlignment(JTextField.RIGHT);
		

		bAdd = new JButton("ADD");
		bDelete = new JButton("DELETE");
		bUpdate= new JButton("UPDATE");
		bView = new JButton("VIEW");
	

	}

	private void addWidgets() {
		hBox1.add(jlID);
		hBox1.add(jtID);
		hBox2.add(jlNameP);
		hBox2.add(jtNameP);
		hBox3.add(jlPrice);
		hBox3.add(jtPrice);
		
		hBox6.add(Box.createHorizontalStrut(82));
		hBox6.add(bAdd);
		hBox6.add(Box.createHorizontalStrut(5));
		hBox6.add(bDelete);
		hBox7.add(Box.createHorizontalStrut(5));
		hBox7.add(bUpdate);
		hBox7.add(Box.createHorizontalStrut(5));
		hBox7.add(bView);
		hBox7.add(Box.createHorizontalStrut(20));
	

		mainBox.add(hBox1);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox2);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox3);
		mainBox.add(Box.createVerticalStrut(5));
	
		mainBox.add(hBox6);
		mainBox.add(Box.createVerticalStrut(5));
		mainBox.add(hBox7);
		mainBox.add(hBox8);
	
	

		add(mainBox);
	}

	public void addActionListener(ActionListener a) {
		bAdd.addActionListener(a);
		bDelete.addActionListener(a);
		bUpdate.addActionListener(a);
		bView.addActionListener(a);
		
	}


	
	public JButton getButtonAdd() {
		return bAdd;
	}

	public JButton getButtonDelete() {
		return bDelete;
	}

	public JButton getButtonUpdate() {
		return bUpdate;
	}

	public JButton getButtonView() {
		return bView;
	}


	public JTextField getTextFieldID() {
		return jtID;
	}

	public JTextField getTextFieldNameP() {
		return jtNameP;
	}
	public JTextField getTextFieldPrice() {
		return jtPrice;
	}

	

}

