package presentation;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Client;
import model.Order;
import model.Product;
import start.ConnectionFactory;




public class TableRows {

	private static String statement1 = "SELECT COUNT(ClientID) AS count FROM clients";
	
	public static int countRowsClient(Client client) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement countC = null;
		ResultSet rs = null;
		int count = 0;
		
			try {
				countC = dbConn.prepareStatement(statement1);
				
				rs = countC.executeQuery();
				rs.next();
				
				count = rs.getInt("count");
				    
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return count;
	}
	
	
}